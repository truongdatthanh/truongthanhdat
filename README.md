# TruongThanhDat

## Library Management software ##
As a library manager, I want to manage readers' borrowing and returning books, so that I can inventory and manage books in more detail.

| Title                  | Manage the library's borrowing-returning list | 
| :----------------------| :---------------------------------------------|  
| **Value Stament**      | As a library manager, I want to manage readers' borrowing and returning books, so that I can inventory and manage books in more detail.                               | 
| **Acceptance Criteria**| _Acceptance Criteria 1:_    - Given valid reader card                                             |  
| H                      | I                                             | 
| T                      | U                                             | 

